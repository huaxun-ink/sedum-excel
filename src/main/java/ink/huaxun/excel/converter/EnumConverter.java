package ink.huaxun.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import ink.huaxun.core.enumeration.BaseEnum;
import ink.huaxun.core.enumeration.ExcelEnum;

/**
 * LocalDate and string converter
 *
 * @author zhubo
 */
public class EnumConverter implements Converter<ExcelEnum> {


    @Override
    public Class<BaseEnum> supportJavaTypeKey() {
        return BaseEnum.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public ExcelEnum convertToJavaData(ReadCellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        Class<?> enumClass = contentProperty.getField().getType();
        ExcelEnum[] enums = (ExcelEnum[]) enumClass.getEnumConstants();
        for (ExcelEnum e : enums) {
            if (e.getNote().equals(cellData.getStringValue())) {
                return e;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<String> convertToExcelData(ExcelEnum value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return new WriteCellData<>(value.getNote());
    }

}
