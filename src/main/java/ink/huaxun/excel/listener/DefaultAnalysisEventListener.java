package ink.huaxun.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import lombok.extern.slf4j.Slf4j;

import javax.validation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 默认的 AnalysisEventListener
 *
 * @author zhubo
 */
@Slf4j
public class DefaultAnalysisEventListener<T> extends ListAnalysisEventListener<T> {

    private final List<T> list = new ArrayList<>();

    private final List<Map<Integer, String>> headMapList = new ArrayList<>();

    private final Map<Long, Set<ConstraintViolation<T>>> errors = new ConcurrentHashMap<>();

    private Long lineNum = 1L;


    private static final Validator VALIDATOR;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    public static <T> Set<ConstraintViolation<T>> validate(T object) {
        return VALIDATOR.validate(object);
    }

    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        lineNum++;

        Set<ConstraintViolation<T>> violations = validate(t);
        if (!violations.isEmpty()) {
            errors.put(lineNum, violations);
        } else {
            list.add(t);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.debug("Excel read analysed");
    }

    @Override
    public List<T> getList() {
        return list;
    }

    @Override
    public List<Map<Integer, String>> getHeadList() {
        return headMapList;
    }

    @Override
    public Map<Long, Set<ConstraintViolation<T>>> getErrors() {
        return errors;
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        headMapList.add(headMap);
    }
}
