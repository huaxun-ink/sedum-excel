package ink.huaxun.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 非模型AnalysisEventListener
 *
 * @author zhubo
 */
@Slf4j
public class NoModelAnalysisEventListener<T> extends ListAnalysisEventListener<Map<Integer, String>> {

    private final static Logger logger = LoggerFactory.getLogger(NoModelAnalysisEventListener.class);

    private final List<Map<Integer, String>> list = new ArrayList<>();

    private final List<Map<Integer, String>> headMapList = new ArrayList<>();

    private final Map<Long, Set<ConstraintViolation<Map<Integer, String>>>> errors = new ConcurrentHashMap<>();

    private Long lineNum = 1L;


    private static final Validator VALIDATOR;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    public static Set<ConstraintViolation<Map<Integer, String>>> validate(Map<Integer, String> object) {
        return VALIDATOR.validate(object);
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext analysisContext) {
        // 跳过头
        if (lineNum >= analysisContext.readRowHolder().getRowIndex()) {
            return;
        }
        lineNum++;
        Set<ConstraintViolation<Map<Integer, String>>> violations = validate(data);
        if (!violations.isEmpty()) {
            errors.put(lineNum, violations);
        } else {
            list.add(data);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.debug("Excel read analysed");
    }

    @Override
    public List<Map<Integer, String>> getList() {
        return list;
    }

    @Override
    public List<Map<Integer, String>> getHeadList() {
        return headMapList;
    }

    @Override
    public Map<Long, Set<ConstraintViolation<Map<Integer, String>>>> getErrors() {
        return errors;
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        logger.info("解析到一条头数据：{}, currentRowHolder: {}", headMap.toString(), context.readRowHolder().getRowIndex());
        lineNum++;
        headMapList.add(headMap);
    }
}
