package ink.huaxun.excel.util;

import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import org.apache.poi.ss.usermodel.*;

/**
 * @author admin
 */
public class ExcelStyleUtils {


    public static HorizontalCellStyleStrategy defaultHorizontalCellStyleConfig() {
        return new HorizontalCellStyleStrategy(defaultHeadWriteCellStyleConfig(), defaultContentWriteCellStyleConfig());
    }


    public static WriteCellStyle defaultHeadWriteCellStyleConfig() {
        return headWriteCellStyleConfig(IndexedColors.PALE_BLUE);
    }

    /**
     * @param indexedColors 单元格颜色
     * @return WriteCellStyle
     */
    public static WriteCellStyle headWriteCellStyleConfig(IndexedColors indexedColors) {
        return headWriteCellStyleConfig(indexedColors, setDefaultHeadWriteFont());
    }

    /**
     * @param indexedColors 单元格颜色
     * @param headWriteFont 单元格字体
     * @return WriteCellStyle
     */
    public static WriteCellStyle headWriteCellStyleConfig(IndexedColors indexedColors, WriteFont headWriteFont) {
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        headWriteCellStyle.setFillForegroundColor(indexedColors.getIndex());
        headWriteCellStyle.setWriteFont(headWriteFont);
        headWriteCellStyle.setWrapped(true);
        return headWriteCellStyle;
    }


    public static WriteCellStyle defaultContentWriteCellStyleConfig() {
        return contentWriteCellStyleConfig(IndexedColors.WHITE);
    }

    public static WriteCellStyle contentWriteCellStyleConfig(IndexedColors indexedColors) {
        return contentWriteCellStyleConfig(indexedColors, setDefaultCellWriteFont());
    }

    public static WriteCellStyle contentWriteCellStyleConfig(IndexedColors indexedColors, WriteFont cellWriteFont) {
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        // 这里需要指定 FillPatternType 为FillPatternType.SOLID_FOREGROUND 不然无法显示背景颜色.头默认了 FillPatternType所以可以不指定
        contentWriteCellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);

        // 背景
        contentWriteCellStyle.setFillForegroundColor(indexedColors.getIndex());
        // 字体策略(暂时使用默认)
        contentWriteCellStyle.setWriteFont(cellWriteFont);
        //设置 自动换行
        contentWriteCellStyle.setWrapped(true);
        //设置 垂直居中
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        //设置 水平居中
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        //设置边框样式
        contentWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        contentWriteCellStyle.setBorderTop(BorderStyle.THIN);
        contentWriteCellStyle.setBorderRight(BorderStyle.THIN);
        contentWriteCellStyle.setBorderBottom(BorderStyle.THIN);
        return contentWriteCellStyle;
    }


    /**
     * 设置默认字体
     *
     * @return WriteFont
     */
    public static WriteFont setDefaultHeadWriteFont() {
        return setWriteFont(null, (short) 16, null, null, null, null, null, null, true);
    }

    /**
     * 设置默认字体
     *
     * @return WriteFont
     */
    public static WriteFont setDefaultCellWriteFont() {
        return setWriteFont(null, (short) 12, null, null, null, null, null, null, true);
    }

    /**
     * @param fontName           字体名称
     * @param fontHeightInPoints 字体大小
     * @param italic             是否为斜体
     * @param strikeout          是否加删除线
     * @param color              颜色
     * @param typeOffset         设置对齐
     * @param underline          设置下划线的粗细
     * @param charset            字符集类型
     * @param bold               是否加粗
     * @return writeFont
     * @see Font
     */
    public static WriteFont setWriteFont(String fontName, Short fontHeightInPoints, Boolean italic, Boolean strikeout, Short color, Short typeOffset, Byte underline, Integer charset, Boolean bold) {
        WriteFont writeFont = new WriteFont();
        writeFont.setFontName(fontName);
        writeFont.setFontHeightInPoints(fontHeightInPoints);
        writeFont.setItalic(italic);
        writeFont.setStrikeout(strikeout);
        writeFont.setColor(color);
        writeFont.setTypeOffset(typeOffset);
        writeFont.setUnderline(underline);
        writeFont.setCharset(charset);
        writeFont.setBold(bold);
        return writeFont;
    }

}
