package ink.huaxun.excel.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import ink.huaxun.excel.listener.DefaultAnalysisEventListener;
import ink.huaxun.excel.listener.ListAnalysisEventListener;
import ink.huaxun.excel.listener.NoModelAnalysisEventListener;
import ink.huaxun.core.exception.CommonException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author zhubo
 */
public class ReadExcelUtils {


    /**
     * 读取单sheet excel
     *
     * @param file      文件
     * @param headClazz 关联类
     * @return list
     */
    public static <T> List<T> readOneSheetExcel(MultipartFile file, Class<T> headClazz) {
        checkFile(file);
        ListAnalysisEventListener<T> readListener = new DefaultAnalysisEventListener<>();
        File tempFile = multipartFileTransToFile(file);
        EasyExcel.read(tempFile, headClazz, readListener).sheet().doRead();
        deleteTempFile(tempFile);
        return readListener.getList();
    }

    /**
     * 读取非对象的单sheet excel
     *
     * @param file 文件
     * @return list
     */
    public static List<Map<Integer, String>> readNoModelSheetExcel(MultipartFile file) {
        return readNoModelSheetExcel(file, 1);
    }

    /**
     * 读取非对象的单sheet excel
     *
     * @param file          文件
     * @param headRowNumber 头的行
     * @return list
     */
    public static List<Map<Integer, String>> readNoModelSheetExcel(MultipartFile file, Integer headRowNumber) {
        checkFile(file);
        ListAnalysisEventListener<Map<Integer, String>> readListener = new NoModelAnalysisEventListener<>();
        File tempFile = multipartFileTransToFile(file);
        EasyExcel.read((File) file, readListener).sheet().headRowNumber(headRowNumber).doRead();
        deleteTempFile(tempFile);
        return readListener.getList();
    }


    private static void checkFile(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (fileName == null) {
            throw new CommonException(500, "文件格式错误！");
        }
        if (!fileName.toLowerCase().endsWith(ExcelTypeEnum.XLS.getValue()) && !fileName.toLowerCase().endsWith(ExcelTypeEnum.XLSX.getValue())) {
            throw new CommonException(500, "文件格式错误！");
        }
    }

    private static File multipartFileTransToFile(MultipartFile file) {
        File toFile = null;
        if (file != null && file.getSize() > 0) {
            InputStream ins;
            try {
                ins = file.getInputStream();
                toFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
                inputStreamToFile(ins, toFile, file.getSize());
                ins.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return toFile;
    }

    private static void inputStreamToFile(InputStream ins, File file, Long size) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead;
            byte[] buffer = new byte[size.intValue()];
            while ((bytesRead = ins.read(buffer, 0, size.intValue())) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void deleteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }
    }

}
