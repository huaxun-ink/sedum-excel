package ink.huaxun.excel.util;

import com.alibaba.excel.EasyExcel;
import ink.huaxun.util.SpringContextUtil;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

/**
 * @author zhubo
 */

@Component
public class WriteExcelUtils {

    public static String getUploadPath() {
        Environment environment = SpringContextUtil.getBean(Environment.class);
        return environment.getProperty("system.path.upload");
    }

    public static <T> String writeExcel(String fileName, Class<T> clazz, List<?> dataList) {
        return writeExcel(fileName, clazz, dataList, "sheet1");
    }

    public static <T> String writeExcel(String fileName, Class<T> clazz, List<?> dataList, String sheetName) {
        String path = getBizFilePath(fileName);
        EasyExcel.write(path, clazz).registerWriteHandler(ExcelStyleUtils.defaultHorizontalCellStyleConfig()).sheet(sheetName).doWrite(dataList);
        return getUrlFilePath(fileName);
    }


    public static String writeExcel(String fileName, List<List<String>> headList, List<?> dataList) {
        return writeExcel(fileName, headList, dataList, "sheet1");
    }

    public static String writeExcel(String fileName, List<List<String>> headList, List<?> dataList, String sheetName) {
        String path = getBizFilePath(fileName);
        EasyExcel.write(path).head(headList).registerWriteHandler(ExcelStyleUtils.defaultHorizontalCellStyleConfig()).sheet(sheetName).doWrite(dataList);
        return getUrlFilePath(fileName);
    }


    private static String getUrlFilePath(String fileName) {
        LocalDate date = LocalDate.now();
        String path = "/excel" + File.separator + date.getYear() + File.separator + date.getMonth() + File.separator;
        return path + File.separator + fileName;
    }

    private static String getBizFilePath(String fileName) {
        LocalDate date = LocalDate.now();
        String path = getUploadPath() + "/excel" + File.separator + date.getYear() + File.separator + date.getMonth() + File.separator;
        String savePath = path + File.separator + fileName;
        createPaths(path);
        return savePath;
    }

    private static void createPaths(String paths) {
        File dir = new File(paths);
        if (!dir.exists()) {
            // 创建目录
            dir.mkdirs();
        }
    }

}
